import { Mongo } from "meteor/mongo";

import authors, { Authors } from './authors'

export const Resolutions = new Mongo.Collection("resolutions");

const schema = `
  type Resolution {
    _id: String
    name: String
    author: Author
  }
`;

export const queries = `
  resolutions: [Resolution]
  resolution(_id: String): Resolution
`;

export const mutations = `
  createResolution(name: String!): Resolution
  removeResolution(_id: String!): Resolution
`;

const resolvers = {
  queries: {
    resolutions() {
      return Resolutions.find({}).fetch();
    },
    resolution(root, { _id }) {
      return Resolutions.findOne(_id);
    }
  },
  mutations: {
    createResolution(obj, { name }, context) {
      const resolutionId = Resolutions.insert({
        name: name
      });

      return Resolutions.findOne(resolutionId);
    },
    removeResolution(obj, { _id }, context) {
      Resolutions.remove(_id);

      return { _id: _id };
    }
  },
  Resolution: {
    author(resolution) {
      return Authors.findOne(resolution.author);
    }
  }
}

export default () => ({
  schema,
  queries,
  mutations,
  resolvers,
  modules: [authors]
})
