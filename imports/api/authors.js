import { Mongo } from "meteor/mongo";

import resolutions from './resolutions'

export const Authors = new Mongo.Collection("authors");

const schema = `
   type Author {
     _id: String
     name: String
     resolutions: [Resolution]
   }
`;

export const queries = `
   authors: [Author]
   author(_id: String): Author
`;

const resolvers = {
  queries: {
    authors() {
      return Authors.find({}).fetch();
    },
    author(root, { _id }) {
      return Authors.findOne(_id);
    }
  },
  Author: {
    resolutions(author) {
      return Resolutions.find({
        "_id": { "$in": author.resolutions }
      });
    }
  }
}

export default () => ({
  schema,
  queries,
  resolvers,
  modules: [resolutions]
})
