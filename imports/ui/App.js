import React, { Component } from "react";
import gql from "graphql-tag";
import { graphql, compose } from "react-apollo";

import ResolutionForm from "./ResolutionForm";

const Fragment = React.Fragment;

class App extends Component {
  removeResolution = (resolution, e) => {
    this.props.removeResolution({
      variables: {
        _id: resolution._id
      }
    })
    .catch(error => {
      console.log(error);
    });
  };

  render() {
    const data = this.props.data;

    if (data.loading) return null;

    return (
      <Fragment>
        <ResolutionForm />
        <ul>
          {data.resolutions.map(resolution => (
            <li key={resolution._id}
              onClick={this.removeResolution.bind(this, resolution)}>{resolution.name}</li>
          ))}
        </ul>
      </Fragment>
    );
  };
}

const resolutionsQuery = gql`
  query Resolutions {
    resolutions {
      _id
      name
      author {
        _id
        name
      }
    }
  }
`;

const resolutionsMutation = gql`
  mutation Resolutions($_id: String!) {
    removeResolution(_id: $_id) {
      _id
    }
  }
`;


export default compose(
  graphql(resolutionsQuery),
  graphql(resolutionsMutation, {
    name: "removeResolution",
    options: {
      refetchQueries: ["Resolutions"]
    }
  })
)(App);
