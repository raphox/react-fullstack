import { createApolloServer } from "meteor/apollo";
import { makeExecutableSchema } from "graphql-tools";
import { bundle } from 'graphql-modules';

import resolutions from '../../api/resolutions';

const modules = [resolutions];
const schema = makeExecutableSchema(bundle(modules));

createApolloServer({
  schema
});
